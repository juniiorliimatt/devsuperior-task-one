package com.devsuperior.api.services;

import com.devsuperior.api.dtos.ClientDTO;
import com.devsuperior.api.entities.Client;
import com.devsuperior.api.repositories.ClientRepository;
import com.devsuperior.api.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

@Service
public class ClientService {

  private final ClientRepository repository;

  @Autowired
  public ClientService(ClientRepository repository) {
	this.repository = repository;
  }

  @Transactional(readOnly = true)
  public Page<ClientDTO> findAll(PageRequest pageRequest) {
	var clients = repository.findAll(pageRequest);
	return clients.map(ClientDTO::new);
  }

  @Transactional(readOnly = true)
  public ClientDTO findById(Long id) {
	var optional = repository.findById(id);
	var client = optional.orElseThrow(() -> new ResourceNotFoundException("Client not found"));
	return new ClientDTO(client);
  }

  @Transactional
  public ClientDTO save(ClientDTO clientDTO) {
	var client = new Client();
	BeanUtils.copyProperties(clientDTO, client);
	client = repository.save(client);
	return new ClientDTO(client);
  }

  @Transactional
  public ClientDTO update(Long id, ClientDTO clientDTO) {
	try {
	  var client = repository.getOne(id);
	  BeanUtils.copyProperties(clientDTO, client);
	  client = repository.save(client);
	  return new ClientDTO(client);
	} catch(ResourceNotFoundException|EntityNotFoundException e) {
	  throw new ResourceNotFoundException("Id " + id + " not found.");
	}
  }

  public void deleteById(Long id) {
	try {
	  repository.deleteById(id);
	} catch(ResourceNotFoundException e) {
	  throw new ResourceNotFoundException("Id " + id + " not found.");
	} catch(DataIntegrityViolationException e) {
	  throw new DataIntegrityViolationException("Integrity violation");
	} catch(EmptyResultDataAccessException e) {
	  throw new EmptyResultDataAccessException(1);
	}
  }

}
