package com.devsuperior.api.controllers;

import com.devsuperior.api.dtos.ClientDTO;
import com.devsuperior.api.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/clients")
public class ClientController {

  private final ClientService service;

  @Autowired
  public ClientController(ClientService service) {
	this.service = service;
  }

  @GetMapping
  public ResponseEntity<Page<ClientDTO>> findAll(
	  @RequestParam(value = "page", defaultValue = "0") Integer page,
	  @RequestParam(value = "linesPerPage", defaultValue = "5") Integer linesPerPage,
	  @RequestParam(value = "direction", defaultValue = "ASC") String direction,
	  @RequestParam(value = "orderBy", defaultValue = "name") String orderBy) {
	PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
	var clientsDTO = service.findAll(pageRequest);
	return ResponseEntity.ok().body(clientsDTO);
  }

  @GetMapping("/{id}")
  public ResponseEntity<ClientDTO> findById(@PathVariable("id") Long id) {
	var clientDTO = service.findById(id);
	return ResponseEntity.ok().body(clientDTO);
  }

  @PostMapping
  public ResponseEntity<ClientDTO> save(@RequestBody ClientDTO clientDTO) {
	var dto = service.save(clientDTO);
	var uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(dto.getId()).toUri();
	return ResponseEntity.created(uri).body(dto);
  }

  @PutMapping("/{id}")
  public ResponseEntity<ClientDTO> update(@PathVariable("id") Long id, @RequestBody ClientDTO clientDTO) {
	var dto = service.update(id, clientDTO);
	return ResponseEntity.ok().body(dto);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<ClientDTO> deleteById(@PathVariable("id") Long id) {
	service.deleteById(id);
	return ResponseEntity.noContent().build();
  }

}
